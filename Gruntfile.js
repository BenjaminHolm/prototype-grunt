module.exports = function(grunt) {

	// measures the time each task takes
	require('time-grunt')(grunt);

	// load grunt config
	require('load-grunt-config')(grunt);

	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
	grunt.registerTask( 'default', ['concurrent:dev'] );
	grunt.registerTask( 'wpl', ['clean:wp', 'compass:wp', 'autoprefixer:wp', 'jade:wp', 'uncss:wp', 'clean:wpHtml', 'rsync:local']);
	grunt.registerTask( 'wpp', ['clean:wp', 'compass:wp', 'autoprefixer:wp', 'jade:wp', 'uncss:wp', 'clean:wpHtml', 'rsync:prod']);
	grunt.registerTask( 'rl', ['rsync:local'] );
	grunt.registerTask( 'rlive', ['rsync:live'] );
	grunt.registerTask( 'rp', ['rsync:prod'] );
};
