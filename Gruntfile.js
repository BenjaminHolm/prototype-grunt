module.exports = function(grunt) {
    grunt.initConfig({

        clean: {
            build: {
                src: ['prototype-codekit', 'prototype-grunt', 'prototype-js', 'prototype-pug', 'prototype-sass', 'prototype-wp']
            }
        },

        copy: {
            buildcodekit: {
                cwd: './prototype-codekit',
                dest: './',
                expand: true,
                src: './*.codekit3'
            },

            buildjs: {
                cwd: './prototype-js',
                dest: './THEME_NAME/assets/js',
                expand: true,
                flatten: true,
                src: './main.js'
            },

            buildpug: {
                cwd: './prototype-pug',
                dest: './build',
                expand: true,
                src: './**/*'
            },

            buildsass: {
                cwd: './prototype-sass',
                dest: './THEME_NAME/assets/sass',
                expand: true,
                src: './**/*'
            },

            buildvendorcss: {
                cwd: './node_modules/',
                dest: './THEME_NAME/assets/vendor',
                expand: true,
                flatten: true,
                src: ['@glidejs/glide/dist/css/glide.core.css']
            },

            buildvendorjs: {
                cwd: './node_modules/',
                dest: './THEME_NAME/assets/vendor',
                expand: true,
                flatten: true,
                src: ['jquery/dist/jquery.js', '@glidejs/glide/dist/glide.js']
            },

            buildwp: {
                cwd: './prototype-wp',
                dest: './THEME_NAME',
                expand: true,
                src: './**/*'
            }

        },

        rsync: {
            options: {
                args: ['--verbose'],
                exclude: ['.git*', 'node_modules'],
                recursive: true,
                ssh: true
            },
            live: {
                options: {
                    src: 'THEME_NAME',
                    dest: '~/public_html/wp-content/themes/',
                    host: '',
                    port: '2222',
                    syncDestIgnoreExcl: true
                }
            },
            local: {
                options: {
                src: 'THEME_NAME',
                dest: '~/Local/THEME_NAME/app/public/wp-content/themes/',
                syncDestIgnoreExcl: true,
                }
            }
        },

        watch: {
            upLocal: {
                options: {spawn: false},
                files: ['THEME_NAME/*'],
                tasks: ['rsync:local']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-rsync');

    grunt.registerTask('build', ['copy:buildcodekit', 'copy:buildjs', 'copy:buildpug', 'copy:buildsass', 'copy:buildvendorcss', 'copy:buildvendorjs', 'copy:buildwp']);
    grunt.registerTask('cleaner', ['clean:build']);
    grunt.registerTask('default', ['watch:upLocal']);

};
