module.exports = {
	dev: {
		src: ['dev/css/style.css']
	},
	dist: {
		src: ['dist/assets/css/style.css']
	},
	wp: {
		src: ['theme_name/style.css']
	}
};
