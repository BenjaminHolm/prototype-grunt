module.exports = {
	dev: {
		options: {
			pretty: true
		},
		files: {
			'dev/index.html': ['jade/index.jade']
		}
	},
	dist: {
		options: {
			pretty: false
		},
		files: {
			'dist/index.html': ['jade/index.jade']
		}
	},
	wp: {
		options: {
			pretty: false
		},
		files: {
			'theme_name/*.html': ['jade/*/**.jade']
		}
	}
};
