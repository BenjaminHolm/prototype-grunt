module.exports = {
	dev: {
		files: {
		'dev/css/style.css': ['dev/index.html']
		}
	},
	dist: {
		files: {
		'dist/assets/css/style.css': 
			[
				'dist/index.html'
			]
		}
	},
	wp: {
		files: {
		'theme_name/style.css': 
			[
				'theme_name/*.html'
			]
		}
	} 
};
