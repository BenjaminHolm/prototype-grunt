module.exports = {
	dist: {
		files: [{
			expand: true,
			cwd: 'dev/img',
			src: ['**/*.svg'],
			dest: 'dist/assets/img',
			ext: '.svg'
		}]
	}
};
