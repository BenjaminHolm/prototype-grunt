module.exports = {
	options: {
		trueColor: true,
		precomposed: true,
		windowsTile: true,
		tileBlackWhite: false,
		tileColor: 'auto',
		html: 'dist/index.html',
		HTMLPrefix: 'assets/img/favicons/'
	},
	icons: {
		src: 'dist/assets/img/favicons/favicon.png',
		dest: 'dist/assets/img/favicons/'
	}
};
