module.exports = {
	dev: {
		files: {
			'dev': ['dev/css/style.css']
		}
	},
	dist: {
		files: {
			'dist': ['dist/assets/css/style.css']
		}
	},
	wp: {
		files: {
			'wp': ['theme_name/style.css']
		}
	}
};
