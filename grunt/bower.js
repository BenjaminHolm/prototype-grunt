module.exports = {
	install: {
		options: {
		targetDir: 'vendor',
		layout: 'byComponent',
		install: true,
		verbose: false,
		cleanTargetDir: false,
		cleanBowerDir: true,
		bowerOptions: {}
		}
	}
};