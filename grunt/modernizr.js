module.exports = {
	'devFile' : 'vendor/modernizr/modernizr.js',
	'outputFile' : 'build/modernizr-custom.js',
	'extra' : {
		'shiv' : true,
		'printshiv' : false,
		'load' : true,
		'mq' : false,
		'cssclasses' : true
	},
	'extensibility' : {
		'addtest' : false,
		'prefixed' : false,
		'teststyles' : false,
		'testprops' : false,
		'testallprops' : false,
		'hasevents' : false,
		'prefixes' : false,
		'domprefixes' : false
	},
	'uglify' : true,
	'tests' : [],
	'parseFiles' : true,
	'matchCommunityTests' : false,
	'customTests' : []
};
