module.exports = {
	dist: {
			src: ['dist/*.html'],
			dest: 'dist/',
			replacements: [{ 
					from: 'css/style',
					to: 'assets/css/style' 
			}, {
					from: 'js/',
					to: 'assets/js/'
			}]
	}
};
