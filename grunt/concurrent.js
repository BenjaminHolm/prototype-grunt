module.exports = {
	options: {
		logConcurrentOutput: true
	},
	dev: {
		tasks: ['newer:watch:css', 'newer:watch:jade']
	}
};