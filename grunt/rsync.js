module.exports = {
	options: {
	args: ["--verbose"],
	exclude: [".git*","*.scss","node_modules"],
	recursive: true,
	ssh: true
	},
	dev: {
		options: {
			src: "wp",
			dest: "/srv/www/SITE/htdocs/wp-content/themes/",
			host: "vagrant@vvv",
			syncDestIgnoreExcl: true,
		}
	},
	live: {
		options: {
			src: "theme_name",
			dest: "~/public_html/wp-core/wp-content/themes/",
			host: "",
			port: "2222",
			syncDestIgnoreExcl: true
		}
	},
	local: {
		options: {
			src: "theme_name",
			dest: "/srv/www/site_name/htdocs/wp-core/wp-content/themes/",
			host: "vagrant@vvv",
			syncDestIgnoreExcl: true,
		}
	},
	prod: {
		options: {
			src: "theme_name",
			dest: "~/public_html/wp-production/wp-core/wp-content/themes/",
			host: "",
			port: "2222",
			syncDestIgnoreExcl: true
		}
	}
};
