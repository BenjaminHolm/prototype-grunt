module.exports = {
	dev: {
		options: {
			browsers: ['last 1 versions']
		},
		files: {
			'dev/css/style.css': 'dev/css/style.css'
		}
	},
	dist: {
		options: {
			browsers: ['last 2 versions', 'ie 8']
		},
		files: {
			'dist/assets/css/style.css': 'dist/assets/css/style.css'
		}
	},
	wp: {
		options: {
			browsers: ['last 2 versions', 'ie 8']
		},
		files: {
			'theme_name/style.css': 'theme_name/style.css'
		}
	}
};
