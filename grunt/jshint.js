module.exports = {
	dev: {
		options: {
			curly: true,
			eqeqeq: true,
			eqnull: true,
			browser: true,
			globals: {
				jQuery: true
			}
		},
		files: {
			src: ['dev/js/main.js']
		}
	}
};
