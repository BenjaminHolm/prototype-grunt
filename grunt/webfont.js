module.exports = {
	icons: {
		dest: 'dev/fonts',
		destCss: 'sass/atoms',
		htmlDemo: false,
		src: 'dev/img/icons/*.svg',
		relativeFontPath: '../fonts',
		options: {
			embed: true,
			font: 'icons',
			stylesheet: 'sass'
		}
	}
};
