module.exports = {
	dev: {
		options: {
			cssDir: 'dev/css',
			cssPath: 'dev/css',
			fontsDir: 'fonts',
			fontsPath: 'dev/fonts',
			httpFontsPath: 'fonts',
			httpImagesPath: 'img',
			httpJavascriptsPath: 'js',
			httpPath: 'dev',
			httpStylesheetsPath: 'css',
			imagesDir: 'dev/img',
			imagesPath: 'dev/img',
			javascriptsDir: 'js',
			javascriptsPath: 'dev/js',
			outputStyle: 'expanded',
			sassDir: 'sass'
		}
	},
	dist: {
		options: {
			cssDir: 'dist/assets/css',
			cssPath: 'dist/assets/css',
			fontsDir: 'assets/fonts',
			fontsPath: 'dist/assets/fonts',
			httpFontsPath: 'assets/fonts',
			httpImagesPath: '../img',
			httpJavascriptsPath: 'assets/js',
			httpPath: 'dist',
			httpStylesheetsPath: 'dist/assets/css',
			imagesDir: 'dist/assets/img',
			imagesPath: 'dist/assets/img',
			javascriptsDir: 'assets/js',
			javascriptsPath: 'dist/assets/js',
			outputStyle: 'compressed',
			sassDir: 'sass'     
		}
	},
	theme_name: {
		options: {
			cssDir: 'theme_name/',
			cssPath: 'theme_name/',
			fontsDir: 'assets/fonts',
			fontsPath: 'theme_name/assets/fonts',
			httpFontsPath: 'theme_name/assets/fonts',
			httpImagesPath: 'theme_name/assets/img',
			httpJavascriptsPath: 'assets/js',
			httpPath: 'theme_name/',
			httpStylesheetsPath: 'theme_name/',
			imagesDir: 'theme_name/assets/img',
			imagesPath: 'theme_name/assets/img',
			javascriptsDir: 'assets/js',
			javascriptsPath: 'theme_name/assets/js',
			outputStyle: 'expanded',
			relativeAssets: true,
			sassDir: 'sass'     
		}
	} 
};