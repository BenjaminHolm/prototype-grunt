module.exports = {
	dev: {
		files: [
		{ src: ['dev/assets/img/**/*.svg'], dest: 'dev/assets/img/' },
		]
	},
	dist: {
		files: [
		{ src: ['dist/assets/img/**/*.svg'], dest: 'dist/assets/img/' },
		]
	}
};
