module.exports = {
	dev: {
		expand: true,
		flatten: true,
		cwd: 'vendor/',
		src: ['**/*.js'],
		dest: 'dev/js',
		filter: 'isFile'
	},
	dist: {
		expand: true,
		flatten: false,
		cwd: 'dev/',
		src: ['css/*', 'fonts/*', 'img/**/*', 'js/*'],
		dest: 'dist/assets/'
	},
	wp: {
		expand: true,
		flatten: false,
		cwd: 'dist/assets',
		src: ['css/*', 'fonts/*', 'img/**/*', 'js/*'],
		dest: 'theme_name/assets/'
	}
};
