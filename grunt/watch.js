module.exports = {

	// development tasks
	css: {
		files: 'sass/**/*.sass',
		tasks: ['compass:dev', 'autoprefixer:dev'],
		options: {
			livereload: false,
			spawn: false
		}
	},
	jade: {
		files: 'jade/**/*.jade',
		tasks: ['jade:dev'],
		options: {
			livereload: false,
			spawn: false
		}
	}
	
	// wordpress tasks
	// css: {
	// 	files: 'sass/**/*.sass',
	// 	tasks: ['compass:wp', 'autoprefixer:wp'],
	// 	options: {
	// 		livereload: false,
	// 		spawn: false
	// 	}
	// },
	// jade: {
	// 	files: 'jade/**/*.jade',
	// 	tasks: ['jade:wp'],
	// 	options: {
	// 		livereload: false,
	// 		spawn: false
	// 	}
	// },
	
	// local rsync
	// upload: {
	// 	files: 'theme_name/*',
	// 	tasks: ['rsync:local']
	// }
};
