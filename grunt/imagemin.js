module.exports = {
	crush: {
		options: {
		optimizationLevel: 5,
		pngquant: true
		},
		files: [{
			expand: true,
			cwd: 'dist/assets/img',
			src: ['**/*.{png,jpg,jpeg,gif}'],
			dest: 'dist/assets/img'
		}]
	}
};
